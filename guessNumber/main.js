        // Program is a number guessing game. Player gets 6 tries to guess a radom number between 1 and 20.
        const readline = require('readline-sync');
        var maxTries = 6;
        var counter = 0;
        
                //Intro
        console.log('Welcome to -- Guess the Number -- game!');
        var name = readline.question('What is your name? ');
        console.log('Hello, ' + name + '. I am thinking of a number between 1 and 20.');
        
                //Generate a random number
        var randomNumber = Math.floor(Math.random() * 20) + 1;
                // Check number -- console.log(RandomNumber);
        
                //Prompt User
        console.log('You get 6 tries to guess the number. '); 
        
        while (guess != randomNumber) {
            var guess = readline.question('Take a guess.');
            counter ++;
        
                if(guess < randomNumber){
                    console.log('Your guess is to low');
                }
                else if (guess > randomNumber){
                    console.log('Your guess is to high');
                }
        
            if(counter > maxTries){
                console.log('Sorry you have no more guesses, you loose.');
                console.log('Would you like to play again?');
                break; 
            }
        
            if (guess == randomNumber){
                console.log('Congratulations, ' + name + '! You WIN!!');
                console.log('The secert number was ' + randomNumber + '.');
                console.log('You guessed my number in ' + counter + ' turns.');
                console.log('Would you like to play again?');
            }   
        }
        